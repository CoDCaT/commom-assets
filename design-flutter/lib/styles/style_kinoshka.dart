
//
// style_kinoshka.dart
//
// Do not edit directly
// Generated on Thu, 25 Feb 2021 14:01:24 GMT
//


import 'dart:ui';

class StyleDictionary {
  StyleDictionary._();
  
    static const color6 = Color(0xFF3A3D42);
    static const colorColor1 = Color(0xFF1A9FDF);
    static const colorColor2 = Color(0xFFFFFFFF);
    static const colorColor3 = Color(0xFF020A24);
    static const colorColor350 = Color(0xFF020A24);
    static const colorColor380 = Color(0xFF1B2942);
    static const colorColor4 = Color(0xFF222B49);
    static const colorColor5 = Color(0xFF000000);
    static const colorColorError = Color(0xFFF11111);
    static const colorColorTransparent = Color(0xFFFFFFFF);
    static const fontFamilySecondaryBold = "InterFace, InterFace-Bold";
    static const fontFamilySecondaryNormal = "InterFace, InterFace-Regular";
    static const fontFamilySmallBold = "InterFace, InterFace-Bold";
    static const fontFamilySmallNormal = "InterFace, InterFace-Regular";
    static const fontFamilySubtitleBold = "InterFace, InterFace-Bold";
    static const fontFamilySubtitleNormal = "InterFace, InterFace-Regular";
    static const fontFamilyTextBold = "InterFace, InterFace-Bold";
    static const fontFamilyTextNormal = "InterFace, InterFace-Regular";
    static const fontFamilyTitleBold = "InterFace, InterFace-Bold";
    static const fontFamilyTitleNormal = "InterFace, InterFace-Regular";
    static const fontLineheightSecondaryBold = "100%";
    static const fontLineheightSecondaryNormal = "100%";
    static const fontLineheightSmallBold = "100%";
    static const fontLineheightSmallNormal = "100%";
    static const fontLineheightSubtitleBold = "100%";
    static const fontLineheightSubtitleNormal = "100%";
    static const fontLineheightTextBold = "100%";
    static const fontLineheightTextNormal = "100%";
    static const fontLineheightTitleBold = "100%";
    static const fontLineheightTitleNormal = "100%";
    static const fontSpacingSecondaryBold = "normal";
    static const fontSpacingSecondaryNormal = "normal";
    static const fontSpacingSmallBold = "normal";
    static const fontSpacingSmallNormal = "normal";
    static const fontSpacingSubtitleBold = "normal";
    static const fontSpacingSubtitleNormal = "normal";
    static const fontSpacingTextBold = "normal";
    static const fontSpacingTextNormal = "normal";
    static const fontSpacingTitleBold = "normal";
    static const fontSpacingTitleNormal = "normal";
    static const fontWeightSecondaryBold = "700";
    static const fontWeightSecondaryNormal = "400";
    static const fontWeightSmallBold = "700";
    static const fontWeightSmallNormal = "400";
    static const fontWeightSubtitleBold = "700";
    static const fontWeightSubtitleNormal = "400";
    static const fontWeightTextBold = "700";
    static const fontWeightTextNormal = "400";
    static const fontWeightTitleBold = "700";
    static const fontWeightTitleNormal = "400";
    static const sizeFontSecondaryBold = 224.00;
    static const sizeFontSecondaryNormal = 224.00;
    static const sizeFontSmallBold = 192.00;
    static const sizeFontSmallNormal = 192.00;
    static const sizeFontSubtitleBold = 384.00;
    static const sizeFontSubtitleNormal = 384.00;
    static const sizeFontTextBold = 256.00;
    static const sizeFontTextNormal = 256.00;
    static const sizeFontTitleBold = 512.00;
    static const sizeFontTitleNormal = 512.00;
}